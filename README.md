# Samply.Common.MDRClient

Samply.Common.MDRClient is a Java library which provides convenient methods
to access metadata from the Samply.MDR. Those  methods provide access to
the metadata of one specific data element or of multiple data elements meeting
some specified criteria.

# Features

- retrieve metadata from Samply.MDR via REST
- access to data elements by URN, data element group or namespace
- access all data elements or only those bound to a specific user (access token
  from Samply.Auth)
- search for data elements

# Repository moved!

As of May 31st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.common.mdrclient. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked MDRClient in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).
